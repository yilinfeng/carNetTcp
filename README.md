#carNetTcp

_实现功能_

约定：
    1，client客户端连接时候，需要按照指定格式发送身份数据：
            identity：message/tag:xxx/alias:xxx/message:xxx
       如果没有指定对应字段，需要发送"空格"作为占位符
            identity:message/tag: /alias: /message:xxx
       服务器通过identity标识每个socket
       
    2，socket与socket 通信，需要走http协议
        例如：localhost:8082/16?message=xxo 
        
message通信走http协议原因
    1，避免冗余的tcp协议解析
    2，发送消息不需要太多的实时性
    3，只要实现推送功能即可
    4，http协议更加通用
    5，方便权限控制
        
          