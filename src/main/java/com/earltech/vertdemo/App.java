package com.earltech.vertdemo;

import com.earltech.vertdemo.socket.TcpServer;
import com.earltech.vertdemo.util.MapUtil;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.net.NetSocket;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.concurrent.ConcurrentHashMap;

/**
 * http -> tcp 服务器，主要流程，http 服务器发送http 请求到Vert.x(Http -> Tcp),Vert.x 直接将消息发送到对应的tcp
 *
 * 服务器主要职责，发送警报信息，发送车辆启动信息，发送
 */
public class App {
    public static void  main(String[] args) {
        //    Runner.runExample(TcpServer.class);
        TcpServer tcpServer = new TcpServer();

        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(tcpServer);

//        HttpServerOptions options = new HttpServerOptions().setMaxWebsocketFrameSize(1000000);

        HttpServer httpServer = vertx.createHttpServer();

        Router router = Router.router(vertx);

//        router.route("/api/whiskies*").handler(BodyHandler.create());
////        router.post("/api/whiskies").handler();
        router.route("/carnettcp/count").handler(routingContext -> {
           tcpServer.getSocketMap().size();
            HttpServerResponse response = routingContext.response();
            response.putHeader("content-type", "text/plain");
            response.end(tcpServer.getBocastMap().size()+""+tcpServer.getSocketMap().size()+tcpServer.getGroupMap().size());
        });

        router.route("/carnettcp/bocast").handler(BodyHandler.create());
        router.route("/carnettcp/bocast").handler(routingContext -> {
            // This handler will be called for every request
            String message = routingContext.request().getParam("message");
            HttpServerResponse response = routingContext.response();
            response.putHeader("content-type", "text/plain");

            ConcurrentHashMap<String, NetSocket> netSocket =tcpServer.getBocastMap();
            if(!netSocket.isEmpty() ){
                netSocket.forEach((key,value)->{value.write(message);});
                response.end("success");
            }else{
                response.end("fail");
            }
        });

        router.route("/carnettcp/group/:groupid").handler(BodyHandler.create());
        router.route("/carnettcp/group/:groupid").handler(routingContext -> {
            // This handler will be called for every request
            String groupId = routingContext.request().getParam("groupid");
            String message = routingContext.request().getParam("message");
            HttpServerResponse response = routingContext.response();
            response.putHeader("content-type", "text/plain");

            ConcurrentHashMap<String, NetSocket> netSocket = MapUtil.startWith(groupId, tcpServer.getGroupMap());
            if(!netSocket.isEmpty() ){
                    netSocket.forEach((key,value)->{value.write(message);});
                    response.end("success");
            }else{
                // Write to the response and end it
                response.end("fail");
            }
        });

        router.route("/carnettcp/single/:userid").handler(BodyHandler.create());
        router.route("/carnettcp/single/:userid").handler(routingContext -> {
            // This handler will be called for every request
            String userid = routingContext.request().getParam("userid");
            String message = routingContext.request().getParam("message");
            HttpServerResponse response = routingContext.response();
            response.putHeader("content-type", "text/plain");

            NetSocket netSocket = tcpServer.getSocketMap().get(userid);
            if(netSocket!= null ){
                if("end" .equals(message)) {
                        netSocket.close();
                        tcpServer.getSocketMap().remove(userid);
                    }else{
                        netSocket.write(message);
                    response.end("success");
                    }
            }else{
                // Write to the response and end it
                response.end("fail");
            }
        });

        httpServer.requestHandler(router::accept).listen(8082,"localhost",res->{
            if(res.succeeded()){
                System.out.println("true");
            }else {
                System.out.println("false");
            }
        });
    }

} 