package com.earltech.vertdemo.util;

import io.vertx.core.net.NetSocket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2016/5/21.
 */
public class MapUtil {

    public static ConcurrentHashMap<String,NetSocket> startWith(String regx,ConcurrentHashMap<String,NetSocket> data ){
        //查找以Java开头,任意结尾的字符串
        Pattern pattern = Pattern.compile("^"+regx+".*");
        //当条件满足时，将返回true，否则返回false
        ConcurrentHashMap<String, NetSocket> hashMap = new ConcurrentHashMap<String,NetSocket>();
        data.forEach((key,value)->{
            Matcher matcher = pattern.matcher(key);
            boolean b= matcher.matches();
            if(b){
                hashMap.put(key,value);
            }
        });
        return hashMap;
    }


}
